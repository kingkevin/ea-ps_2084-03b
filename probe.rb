#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 2.2 
require 'serialport'
require './telegram'

@serial = SerialPort.open("/dev/ttyACM0",{ baud: 115200, databits: 8, parity: SerialPort::ODD, stop_bit: 1, flow_control: SerialPort::NONE})
@serial.dtr = 1

def send(object,data=nil)
  telegram_out = Telegram.new(object,data)
  @serial.write(telegram_out.pack)
  data = @serial.readpartial(1024)
  telegram_in = nil
  if data and !data.empty? then
    begin
      telegram_in = Telegram.parse(data)
      if telegram_in.object==0xff and telegram_in.data[0]==0x07 then
        puts "object not defined"
      else
        puts "< "+telegram_out.pack.unpack("C*").collect { |b| sprintf("%02X ",b) }.join
        puts telegram_out
        puts "> "+data.unpack("C*").collect { |b| sprintf("%02X ",b) }.join
        puts telegram_in
      end
    rescue Exception => e
      puts "> "+data.unpack("C*").collect { |b| sprintf("%02X ",b) }.join
      puts e.to_s
    end
  else
    puts "empty"
  end
end

send(54,[0x10,0x10]) # enable remote control
send(150,[0x1F,0x35,0x57,0xBC]) # give access permissions (hidden command)
send(151,[0xFF,0xAA,0x88,0x44]) # give access permissions (hidden command)

256.times do |object|
  puts "object: #{object}"
  send(object)
end

