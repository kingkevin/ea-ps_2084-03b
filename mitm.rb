#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 1.9
require 'serialport'
require './telegram'

pc_serial = SerialPort.open("/dev/ttyUSB0",{ baud: 57600, databits: 8, parity: SerialPort::ODD, stop_bit: 1, flow_control: SerialPort::NONE})
ps_serial = SerialPort.open("/dev/ttyACM0",{ baud: 115200, databits: 8, parity: SerialPort::ODD, stop_bit: 1, flow_control: SerialPort::NONE})

# ensure serial settings (weird bug)
# the different baudrates used
baudrates = []
baudrates << {baud: 57600, parity: SerialPort::ODD}
baudrates << {baud: 115200, parity: SerialPort::ODD}
baudrate_i = 0

while true
  activities = IO.select([pc_serial,ps_serial])
  activities[0].each do |activity|
    data = activity.readpartial(1024)
    line = data.unpack("C*").collect { |b| sprintf("%02X ",b) }.join
    if activity==pc_serial then
      puts "pc->ps: "+line
      begin
        telegram = Telegram.parse(data)
        if telegram then
          puts "pc->ps: "+telegram.to_s if telegram
          ps_serial.write(data)
        else
          baudrate_i = (baudrate_i+1)%baudrates.size
          pc_serial.baud = baudrates[baudrate_i][:baud]
          pc_serial.parity = baudrates[baudrate_i][:parity]
          puts "could not parse message: switching baudrate to #{pc_serial.baud} bps"
        end
      rescue Exception => e
        puts e.to_s
      end
    elsif activity==ps_serial then
      puts "ps->pc: "+line
      begin
        telegram = Telegram.parse(data)
        puts "ps->pc: "+telegram.to_s if telegram
      rescue Exception => e
        puts e.to_s
      end
      pc_serial.write(data)
    else
      raise "unknown source"
    end
  end
end

