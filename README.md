scripts to communicate with EA-PS 2000 B family power supplies (i.e. EA-PS 2084-03 B)

links
=====

manufacturer: EA elektro-automatik http://www.elektroautomatik.de/en/
product: PS 2000 B http://shop.elektroautomatik.de/shop/shop__Series%20PS%202000B__1::4::14::42__en_GB
driver: USB ACM http://www.elektroautomatik.de/de/ps2000b.html
tool: easyPS2000 http://www.elektroautomatik.de/en/easyps2000-en.html
programming manual: http://www.elektroautomatik.de/files/eautomatik/treiber/ps2000b/programming_ps2000b.zip

files
=====

telegram.rb
-----------

library to create, parse, and decode telegrams used to communicate with the power supply

demo.rb
-------

demonstration script.
read power supply information, set to 42 V for 10 s, and read every second the set and actual values

control.rb
----------

control the power supply to go from 0 V to 84 V in 0.1 V steps (@ 1 A) and read set, actual, and measured values using two multi-meters.
the two DMM are UNI-T UT61E, one connected using the UT02 cable and measuring voltage, the other using the UT04 cable and measuring amapere.
to read the DMM values sigrock-cli is used.

probe.rb
--------

test which object in telegrams exist.
this script is used to detect hidden commands

hidden-commands.txt
-------------------

the resuslts from probe.rb

mitm.rb
-------

small script to monitor the communication:
- connect a windows compute over serial to this compute (using port ttyUSB0)
- connect the power supply to this computer (using port ttyACM0)
- start the mitm.rb script on this computer
- start easyPS2000 software on windows computer (it will use the serial link to this computer)
